#!/usr/bin/env python3

import toml
import sys
import os

def main(in_fd, out_fd):
    x = toml.load(in_fd)
    toml.dump(x, out_fd)

def wrap_stdio(func=main):
    in_arg = '-' if len(sys.argv) < 2 else sys.argv[1]
    out_arg = '-' if len(sys.argv) < 3 else sys.argv[2]
    in_is_std = in_arg == '-'
    out_is_std = out_arg == '-'

    if in_is_std and out_is_std:
        return func(sys.stdin, sys.stdout)
    elif in_is_std:
        with open(sys.argv[2], "w") as out_fd:
            return func(sys.stdin, out_fd)
    else:
        with open(sys.argv[1]) as in_fd:
            if out_is_std:
                return func(in_fd, sys.stdout)
            else:
                with open(sys.argv[2], "w") as out_fd:
                    return func(in_fd, out_fd)

wrap_stdio()