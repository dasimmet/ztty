FROM docker.io/ubuntu:22.04 AS base

FROM base AS tools

RUN apt-get update && apt-get install -y curl git tar

WORKDIR /root

FROM tools AS spin

# RUN curl -fsSL https://developer.fermyon.com/downloads/install.sh | bash
ENV SPIN_RELEASE=canary
RUN curl -OL https://github.com/fermyon/spin/releases/download/${SPIN_RELEASE}/spin-${SPIN_RELEASE}-static-linux-amd64.tar.gz
RUN tar xvf spin-${SPIN_RELEASE}-static-linux-amd64.tar.gz
# RUN ./spin plugin install -y cloud

FROM tools AS ncdu

RUN curl -OSsL https://dev.yorhel.nl/download/ncdu-2.3-linux-x86_64.tar.gz
RUN tar xf ncdu-2.3-linux-x86_64.tar.gz

FROM base AS final

RUN apt-get update && apt-get install -y python3-toml && \
    apt-get clean && rm -f /var/cache/apt/*.bin

COPY --from=spin /root/spin /bin/spin

FROM final AS debug

COPY --from=ncdu /root/ncdu /bin/ncdu

FROM final